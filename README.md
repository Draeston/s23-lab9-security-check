## Test 1 - Forgot password

| №  | Step description                                                                                         | Status | Comment                         |
|----|----------------------------------------------------------------------------------------------------------|--------|---------------------------------|
| 1  | Opened Sign In/Sign Up modal                                                                             | OK     |                                 |
| 2  | Clicked on the 'Sign In' button                                                                          | OK     |                                 |
| 3  | Clicked on the 'Forgot' button                                                                           | OK     |                                 |
| 4  | Tried to enter completely incorrect email                                                                | OK     | ![1.png](1.png)                 |
| 5  | Tried to avoid previous error, by entering correct inexistent email                                      | OK     | ![2.png](2.png)                 |
| 6  | Entered correct email                                                                                    | OK     | ![3.png](3.png) ![4.png](4.png) |
| 7  | Got an email with reset password via URL token                                                           | OK     | ![5.png](5.png)                 |
| 8  | Succesfully changed password                                                                             | OK     |                                 |
| 9  | Tried request from foreign origin, it allowed this request                                               | Not OK | ![6.png](6.png)                 |
| 10 | Check if there is any security measures against flooding, used script below, it stopped after ~30 emails | Ok     | ![7.png](7.png) ![8.png](8.png) |

```
formData = new FormData()
formData.append('email', 'blizza.x@yandex.ru')
formData.append('successRedirect', 'https://learn.javascript.ru/')
for (let i = 0; i < 3000; i++)
  fetch('https://learn.javascript.ru/auth/forgot', 
    {
        method: 'POST', 
        body: formData
    }
  )
```

Security measures for 'Forgot Password' are present, except for not allowing cross-origin, but it is more headers issue

## Test 2 - File upload

| № | Step description                                 | Status | Comment                                               |
|---|--------------------------------------------------|--------|-------------------------------------------------------|
| 1 | Sign In to the site                              | OK     |                                                       |
| 2 | Go to profile settings                           | OK     |                                                       |
| 3 | Click on the button to upload avatar             | OK     | ![9.png](9.png)                                       |
| 4 | Uploaded avatar                                  | OK     | ![10.png](10.png)                                     |
| 5 | Tried to upload wrong avatar masked by extension | OK     | ![11.png](11.png) ![12.png](12.png) ![13.png](13.png) |

Correct validation of file type and correct work of the file uploading itself

## Test 3 - HTTP headers

| № | Step description        | Status | Comment           |
|---|-------------------------|--------|-------------------|
| 1 | Open DevTools > Network | OK     |                   |
| 2 | Reload page             | OK     |                   |
| 3 | Check Response Headers  | Not OK | ![14.png](14.png) |

learn.javascript.com doesn't provide important headers such as X-Frame-Options, X-XSS-Protection, Referrer-Policy, Strict-Transport-Security (HSTS), Content-Security-Policy (CSP), etc.
But other important headers (X-Content-Type-Options, Content-Type, Set-Cookie, Server) are in proper form

## Test 4 - XSS Filter Evasion

| № | Step description                                          | Status | Comment                             |
|---|-----------------------------------------------------------|--------|-------------------------------------|
| 1 | XSS Locator (Polygot)                                     | OK     | ![15.png](15.png) ![16.png](16.png) |
| 2 | Malformed A Tags                                          | OK     | ![17.png](17.png) ![16.png](16.png) |
| 3 | Malformed IMG Tags                                        | OK     | ![18.png](18.png) ![16.png](16.png) |
| 4 | Default SRC Tag to Get Past Filters that Check SRC Domain | OK     | ![19.png](19.png) ![16.png](16.png) |
| 5 | Decimal HTML Character References                         | OK     | ![20.png](20.png) ![16.png](16.png) |
| 6 | Embedded Tab                                              | OK     | ![21.png](21.png) ![16.png](16.png) |

Tested some XSS attacks, but I guess that security measures of this website against those attacks are implemented according to the (at least some) OWASP standards.